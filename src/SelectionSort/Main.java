package SelectionSort;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {2,7,1,4};
		displayArray(selectionSort(arr));

	}
	public static int[] selectionSort(int[] arr) {
		int length = arr.length;
		int indexToBeReplaced = 0;
		for(int j = 0;j<length;) {
			int small = arr[j];
			for(int i = j;i<length;i++) {
				if(arr[i] < small) {
					small = arr[i];
					indexToBeReplaced = i;
				}
			}
			if(small < arr[j]) {
				arr[indexToBeReplaced] = arr[j];
				arr[j] = small;
			}
			j++;
		}
		return arr;	
		
	}
	public static void displayArray(int[] arr) {
		int length = arr.length;
		for(int i = 0;i<length;i++) {
			if(i == length-1) {
				System.out.print(arr[i]);
			}
			else {
				System.out.print(arr[i] + ",");
			}
		}
	}
	

}
